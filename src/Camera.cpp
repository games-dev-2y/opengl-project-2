#include "Camera.h"


/// <summary>
/// Default constructor
/// constructs camera to view from
/// passed position towards target
/// </summary>
/// <param name="position"> 3d vector of the cameras position </param>
/// <param name="target"> 3d vector of the cameras target </param>
Camera::Camera(
	const glm::vec3 & position
	, const glm::vec3 & target
)

	: m_position(position)
	, m_lookingAt(target)
	, m_camera(glm::lookAt(
		m_position,	// Camera (x,y,z), in World Space
		m_lookingAt,	// Camera looking at origin
		m_LOOK_UP	// 0.0f, 1.0f, 0.0f Look Down and 0.0f, -1.0f, 0.0f Look Up
	))
	, m_CAMERA(m_camera)
{
}

/// <summary>
/// Default destructor
/// </summary>
Camera::~Camera()
{
}

/// <summary>
/// Will move the camera by a corresponding offset
/// </summary>
/// <param name="offset"> 3d vector of the offset </param>
void Camera::move(const glm::vec3 & offset)
{
	m_position += offset;
	m_camera = glm::translate(m_camera, offset);
}

/// <summary>
/// Will cause the camera to look at the corresponding target
/// </summary>
/// <param name="target"> 3d vector of the targets position </param>
void Camera::lookAt(const glm::vec3 & target)
{
	m_lookingAt = target;
	m_camera = glm::lookAt(m_position, m_lookingAt, m_LOOK_UP);
}

