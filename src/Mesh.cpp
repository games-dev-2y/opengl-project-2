#include "Mesh.h"



Mesh::Mesh(
	const std::vector<Vertex>& vertices
	, const std::vector<GLuint>& indices
	, const std::vector<Texture>& textures

)
	: m_vertices(vertices)
	, m_indices(indices)
	, m_textures(textures)
{
	setupMesh();
}


Mesh::~Mesh()
{
}

void Mesh::draw(const Shader& shader)
{
	// Bind appropriate textures
	GLuint diffuseNr = 1u;
	GLuint specularNr = 1u;

	for (GLuint i = 0u; i < this->m_textures.size(); i++)
	{
		// Activate the proper texture unit	before binding
		glActiveTexture(GL_TEXTURE0 + i);

		// Retrieve texture number (the N in diffuse_textureN)
		std::stringstream ss;
		std::string number;
		std::string name = this->m_textures[i].m_type;
		if (name == "texture_diffuse")
		{
			ss << diffuseNr++; // Transfer GLuint to string
		}
		else if (name == "texture_specular")
		{
			ss << specularNr++; // Transfer GLuint to string
		}
		number = ss.str();

		// Now set the sampler to the correct texture unit
		glUniform1i(glGetUniformLocation(shader.m_program, (name + number).c_str()), i);

		// and finally bind the texture
		glBindTexture(GL_TEXTURE_2D, this->m_textures[i].m_id);
	}

	// Default mesh's shininess property
	glUniform1f(glGetUniformLocation(shader.m_program, "material_shininess"), 16.0f);

	// Draw mesh
	glBindVertexArray(this->m_vao);
	glDrawElements(GL_TRIANGLES, this->m_indices.size(), GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0u);

	// Good practice to reset textures to avoid undefined behaviour
	for (GLuint i = 0u; i < this->m_textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0u);
	}
}

void Mesh::setupMesh()
{
	/* Generating */
	// vertex array object
	glGenVertexArrays(1, &this->m_vao);
	// vertex buffer object
	glGenBuffers(1, &this->m_vbo);
	// element buffer object
	glGenBuffers(1, &this->m_ebo);

	// binding vertex array object
	glBindVertexArray(this->m_vao);
	// binding vertex buffer object
	glBindBuffer(GL_ARRAY_BUFFER, this->m_vbo);

	// upload vbo data
	glBufferData(
		GL_ARRAY_BUFFER
		, this->m_vertices.size() * sizeof(Vertex)
		, reinterpret_cast<const void*>(&this->m_vertices[0])
		, GL_STATIC_DRAW
	);

	// binding element buffer object
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->m_ebo);
	
	// upload ebo data
	glBufferData(
		GL_ELEMENT_ARRAY_BUFFER
		, this->m_indices.size() * sizeof(GLuint)
		, reinterpret_cast<const void*>(&this->m_indices[0])
		, GL_STATIC_DRAW
	);

	// Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
		reinterpret_cast<const void*>(0)
	);

	// Vertex Normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
		reinterpret_cast<const void*>(offsetof(Vertex, m_normal))
	);

	// Vertex Texture Coords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
		reinterpret_cast<const void*>(offsetof(Vertex, m_texCoords))
	);

	glBindVertexArray(0);
}
