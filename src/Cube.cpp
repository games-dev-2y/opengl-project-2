#include "Cube.h"



Cube::Cube(
	const sf::Time& elapsed
	, const glm::mat4& projectionMat
	, const glm::mat4& lViewMat
	, const glm::mat4& rViewMat
	, const GLchar* vertShader
	, const GLchar* fragShader
	, const GLchar* model)

	: deltaTime(elapsed.asSeconds())
	, m_projectionMat(projectionMat)
	, m_eyeLeftMat(lViewMat)
	, m_eyeRightMat(rViewMat)
	, m_identMat(1.0f)
	, m_normalMat(1.0f)
	, m_modelMat(1.0f)
	, m_shader(vertShader, fragShader)
	, m_model(model)
	, m_position(0.0f)
	, m_size(1.0f, 1.0f, 1.0f)
	, m_ids()
{
	m_normalMat = glm::scale(m_normalMat, m_size);
	m_shader.Use();

	m_ids = std::make_unique<IDs>(
		glGetUniformLocation(m_shader.m_program, "projection")
		, glGetUniformLocation(m_shader.m_program, "view")
		, glGetUniformLocation(m_shader.m_program, "model")
		);

}


Cube::~Cube()
{
}

void Cube::update()
{
	m_normalMat = glm::rotate(m_normalMat, (5.0f * deltaTime), glm::vec3(0.0f, 1.0f, 0.0f));
}

void Cube::render()
{
	m_modelMat = glm::translate(m_normalMat, m_position);
	m_shader.Use();
	
	glViewport(0, 0, 400, 600);
	glUniformMatrix4fv(m_ids->m_projectionID, 1, GL_FALSE, (&m_projectionMat[0][0]));
	glUniformMatrix4fv(m_ids->m_viewID, 1, GL_FALSE, (&m_eyeLeftMat[0][0]));
	glUniformMatrix4fv(m_ids->m_modelID, 1, GL_FALSE, (&m_modelMat[0][0]));
	
	m_model.draw(m_shader);

	glViewport(400, 0, 400, 600);
	glUniformMatrix4fv(m_ids->m_projectionID, 1, GL_FALSE, (&m_projectionMat[0][0]));
	glUniformMatrix4fv(m_ids->m_viewID, 1, GL_FALSE, (&m_eyeRightMat[0][0]));
	glUniformMatrix4fv(m_ids->m_modelID, 1, GL_FALSE, (&m_modelMat[0][0]));

	m_model.draw(m_shader);
}

const glm::vec3 & Cube::position()
{
	return m_position;
}
