#include "Game.h"


/// <summary>
/// Game constructor used to initialize the game and all game components
/// </summary>
Game::Game(sf::ContextSettings settings)
	: m_window(sf::VideoMode(800u, 600u, 32u), "VR Cube", sf::Style::Default, settings)
	, m_leftCam(m_LEFT_CAM_POS, m_CAM_TARGET)
	, m_rightCam(m_RIGHT_CAM_POS, m_CAM_TARGET)
	, m_gameTime(sf::Time::Zero)
	, m_state(States::Game)
	, m_keyhandler()
{

	DEBUG_MSG(glGetString(GL_VENDOR));
	DEBUG_MSG(glGetString(GL_RENDERER));
	DEBUG_MSG(glGetString(GL_VERSION));
	DEBUG_MSG("");

	glewInit();

	auto seed = time(nullptr);

	DEBUG_MSG("Generating random seed:\n   - Seed: " + std::to_string(seed) + "\n");
	srand(static_cast<unsigned int>(seed));

	initialize();
}

/// <summary>
/// Game destructor
/// used to destroy all stack data
/// </summary>
Game::~Game()
{
}

/// <summary>
/// Game loop
/// , each update is called 60 times per second
/// , render is statically called after a full game loop
/// </summary>
void Game::run()
{
	while (m_window.isOpen())
	{
		processEvents();
		m_elapsed += m_clock.restart();
		while (m_elapsed > TIME_PER_FRAME)
		{
			m_elapsed -= TIME_PER_FRAME;
			update();
		}
		render();
	}
}

/// <summary>
/// Processes all window events
/// </summary>
void Game::processEvents()
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		switch (m_state)
		{
		case Game::States::Game:
		default:
			switch (event.type)
			{
			case sf::Event::Closed:
				m_window.close();
				break;
			case sf::Event::KeyPressed:
				m_keyhandler.updateKey(event.key.code, true);
				break;
			case sf::Event::KeyReleased:
				m_keyhandler.updateKey(event.key.code, false);
				break;
			default:
				break;
			}
			break;
		}
	}
}

/// <summary>
/// game update logic
/// </summary>
void Game::update()
{
	switch (m_state)
	{
	case Game::States::Game:
		m_gameTime += TIME_PER_FRAME;
		
		processInput();

		m_cube->update();

		break;
	default:
		break;
	}

}

/// <summary>
/// game render logic
/// </summary>
void Game::render()
{
	switch (m_state)
	{
	case Game::States::Game:
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		m_cube->render();

		m_window.display();
		break;
	default:
		break;
	}

}

void Game::initialize()
{
	loadAssets();

	// Enable Depth test
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
}


void Game::loadAssets()
{
	// create cube
	m_cube = std::make_unique<Cube>(
		TIME_PER_FRAME
		, m_PROJECTION
		, m_leftCam.m_CAMERA
		, m_rightCam.m_CAMERA
		, m_MODEL_VERTEX_SHADER_FILE.c_str()
		, m_MODEL_FRAG_SHADER_FILE.c_str()
		, m_CUBE_OBJ.c_str()
		);
}

void Game::processInput()
{
	const auto MOVE = 0.1f;

	const auto LEFT_CAM_MOVE_LEFT = sf::Keyboard::A;
	const auto LEFT_CAM_MOVE_RIGHT = sf::Keyboard::D;
	const auto LEFT_CAM_MOVE_UP = sf::Keyboard::W;
	const auto LEFT_CAM_MOVE_DOWN = sf::Keyboard::S;

	const auto RIGHT_CAM_MOVE_LEFT = sf::Keyboard::J;
	const auto RIGHT_CAM_MOVE_RIGHT = sf::Keyboard::L;
	const auto RIGHT_CAM_MOVE_UP = sf::Keyboard::I;
	const auto RIGHT_CAM_MOVE_DOWN = sf::Keyboard::K;

	// Left camera movement

	if (m_keyhandler.isPressed(LEFT_CAM_MOVE_LEFT))
	{
		m_leftCam.move(glm::vec3(-MOVE, 0.0f, 0.0f));
		m_leftCam.lookAt(m_cube->position());
	}
	if (m_keyhandler.isPressed(LEFT_CAM_MOVE_RIGHT))
	{
		m_leftCam.move(glm::vec3(MOVE, 0.0f, 0.0f));
		m_leftCam.lookAt(m_cube->position());
	}
	if (m_keyhandler.isPressed(LEFT_CAM_MOVE_UP))
	{
		m_leftCam.move(glm::vec3(0.0f, MOVE, 0.0f));
		m_leftCam.lookAt(m_cube->position());
	}
	if (m_keyhandler.isPressed(LEFT_CAM_MOVE_DOWN))
	{
		m_leftCam.move(glm::vec3(0.0f, -MOVE, 0.0f));
		m_leftCam.lookAt(m_cube->position());
	}

	// Right camera movement

	if (m_keyhandler.isPressed(RIGHT_CAM_MOVE_LEFT))
	{
		m_rightCam.move(glm::vec3(-MOVE, 0.0f, 0.0f));
		m_rightCam.lookAt(m_cube->position());
	}
	if (m_keyhandler.isPressed(RIGHT_CAM_MOVE_RIGHT))
	{
		m_rightCam.move(glm::vec3(MOVE, 0.0f, 0.0f));
		m_rightCam.lookAt(m_cube->position());
	}
	if (m_keyhandler.isPressed(RIGHT_CAM_MOVE_UP))
	{
		m_rightCam.move(glm::vec3(0.0f, MOVE, 0.0f));
		m_rightCam.lookAt(m_cube->position());
	}
	if (m_keyhandler.isPressed(RIGHT_CAM_MOVE_DOWN))
	{
		m_rightCam.move(glm::vec3(0.0f, -MOVE, 0.0f));
		m_rightCam.lookAt(m_cube->position());
	}

	// Both camera movement

	if (m_keyhandler.isPressed(sf::Keyboard::Left))
	{
		m_leftCam.move(glm::vec3(-MOVE, 0.0f, 0.0f));
		m_rightCam.move(glm::vec3(-MOVE, 0.0f, 0.0f));
		m_leftCam.lookAt(m_cube->position());
		m_rightCam.lookAt(m_cube->position());
	}
	if (m_keyhandler.isPressed(sf::Keyboard::Right))
	{
		m_leftCam.move(glm::vec3(MOVE, 0.0f, 0.0f));
		m_rightCam.move(glm::vec3(MOVE, 0.0f, 0.0f));
		m_leftCam.lookAt(m_cube->position());
		m_rightCam.lookAt(m_cube->position());
	}
	if (m_keyhandler.isPressed(sf::Keyboard::Up))
	{
		m_leftCam.move(glm::vec3(0.0f, MOVE, 0.0f));
		m_rightCam.move(glm::vec3(0.0f, MOVE, 0.0f));
		m_leftCam.lookAt(m_cube->position());
		m_rightCam.lookAt(m_cube->position());
	}
	if (m_keyhandler.isPressed(sf::Keyboard::Down))
	{
		m_leftCam.move(glm::vec3(0.0f, -MOVE, 0.0f));
		m_rightCam.move(glm::vec3(0.0f, -MOVE, 0.0f));
		m_leftCam.lookAt(m_cube->position());
		m_rightCam.lookAt(m_cube->position());
	}
}
