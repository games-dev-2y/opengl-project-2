# README #

This VR Project.

### What is this repository for? ###

* OpenGL Project 2 - VR Project)
* Version 1.0

### How do I get set up? ###

* Ensure the SFML_SDK environment variable exists
* Ensure the OPENGL_SDK environment variable exists
* ensure SFML Version SFML 2.3.2 Visual C++ 14 (2015) - 32-bit is installed
* ([http://www.sfml-dev.org/files/SFML-2.3.2-windows-vc14-32-bit.zip "SFML-2.3.2-windows-vc14-32-bit.zip"](http://www.sfml-dev.org/files/SFML-2.3.2-windows-vc14-32-bit.zip))

### Who do I talk to? ###

* [Rafael Plugge](mailto:rafael.plugge@email.com)