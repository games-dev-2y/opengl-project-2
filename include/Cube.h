#pragma once

#include <memory>

#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"

#include "SFML\System\Time.hpp"

#include "Model.h"

/// <summary>
/// @author Rafael Plugge
/// @version 1.0
/// @brief Represents a 3D Cube.
/// 
/// 
/// </summary>
class Cube
{
public:
	Cube(
		const sf::Time& elapsed
		, const glm::mat4& projectionMat
		, const glm::mat4& lViewMat
		, const glm::mat4& rViewMat
		, const GLchar* vertShader
		, const GLchar* fragShader
		, const GLchar* model);
	~Cube();

	void update();
	void render();
	const glm::vec3& position();

private:

	const float deltaTime;
	
	/* 3D MODEL MATRIXS */
	// Projection matrix
	const glm::mat4& m_projectionMat;
	// Left Camera matrix
	const glm::mat4& m_eyeLeftMat;
	// Right Camera matrix
	const glm::mat4& m_eyeRightMat;
	// identity matrix
	const glm::mat4 m_identMat;
	// normal matrix
	glm::mat4 m_normalMat;
	// Model matrix
	glm::mat4 m_modelMat;

	// 3D Models shader
	Shader m_shader;
	// 3D Model
	Model m_model;

	// cube position
	glm::vec3 m_position;
	// cube size
	glm::vec3 m_size;

	struct IDs
	{
		const GLint m_projectionID;
		const GLint m_viewID;
		const GLint m_modelID;

		IDs(
			const GLint& projectionID
			, const GLint& viewID
			, const GLint& modelID)

			: m_projectionID(projectionID)
			, m_viewID(viewID)
			, m_modelID(modelID)
		{}
	};

	// unique pointer to our id struct
	std::unique_ptr<IDs> m_ids;

};

