#ifndef GAME
#define GAME

// used for display debug messages
//  to console
#include "Debug.h"

// opengl wrangler library
#include "GL\glew.h"
#include "GL\wglew.h"

// opengl math library
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"

// SFML Window and OpenGL libraries
#include "SFML\Window.hpp"
#include "SFML\OpenGL.hpp"
#include "SFML\Graphics.hpp"

// standard memory library
// needed for using smart pointers
#include <memory>

// Project classes
#include "KeyHandler.h"
#include "Camera.h"
#include "Model.h"
#include "Cube.h"

/// <summary>
/// Main Entry Point for Game Application
/// </summary>
class Game
{
public:
	Game(sf::ContextSettings settings = sf::ContextSettings());
	~Game();
	void run();
private:
	void processEvents();
	void update();
	void render();

	void initialize();

	void loadAssets();
	void processInput();

	enum class States
	{
		Game
	}m_state;

	// Main Game window
	sf::RenderWindow m_window;

	// main game internal clock
	sf::Clock m_clock;
	// time elapsed between last update call
	sf::Time m_elapsed;

	/* Game constants */
	// time between each frame
	const sf::Time TIME_PER_FRAME = sf::seconds(1.0f / 60.0f);
	// wait time in splash screen
	const sf::Time WAIT_TIME = sf::seconds(3.0f / 60.0f);
	// Game boundaries
	const glm::vec2 m_GAME_BOUNDS = glm::vec2(8.0f, 8.0f);

	/* File path to our shaders */
	// path for all resources
	const std::string m_FILE_PATH = ".\\resources\\";
	// path for all shaders
	const std::string m_SHADER_FOLDER = m_FILE_PATH + "shaders\\";
	// path for all textures
	const std::string m_TEXTURE_FOLDER = m_FILE_PATH + "textures\\";
	// path for all fonts
	const std::string m_FONT_FOLDER = m_FILE_PATH + "fonts\\";
	// path for all models
	const std::string m_MODEL_FOLDER = m_FILE_PATH + "models\\";
	// model Vertex Shader
	const std::string m_MODEL_VERTEX_SHADER_FILE = m_SHADER_FOLDER + "model.vert";
	// model Fragment Shader
	const std::string m_MODEL_FRAG_SHADER_FILE = m_SHADER_FOLDER + "model.frag";
	// font
	const std::string m_FONT_FILE = m_FONT_FOLDER + "ARBERKLEY.ttf";
	// cube model
	const std::string m_CUBE_OBJ = m_MODEL_FOLDER + "cube/cube.obj";

	/* default cube parameters */
	// starting position
	const glm::vec3 START_POS = glm::vec3(0.0f, -2.0f, 3.0f);
	// default width
	const float START_WIDTH = 1.0f;
	// default height
	const float START_HEIGHT = 1.0f;
	// default depth
	const float START_DEPTH = 1.0f;
	/* Game constants */

	/* default background parameter */
	// position
	const glm::vec3 m_BG_POS = glm::vec3(0.0f, 0.0f, -4.0f);
	// default width
	const float m_BG_WIDTH = 12.0f;
	// default height
	const float m_BG_HEIGHT = 12.0f;

	/* default camera parameters */
	// projection
	const glm::mat4 m_PROJECTION = glm::perspective(
		45.0f,					// Field of View 45 degrees
		2.0f / 3.0f,			// Aspect ratio
		1.0f,					// Display Range Min : 0.1f unit
		100.0f					// Display Range Max : 100.0f unit
	);
	// position
	const glm::vec3 m_LEFT_CAM_POS = glm::vec3(0.0f, 6.0f, 10.0f);
	const glm::vec3 m_RIGHT_CAM_POS = glm::vec3(0.0f, 6.0f, 10.0f);
	const glm::vec3 m_CAM_TARGET = glm::vec3(0.0f, 0.0f, 0.0f);

	// Left Camera for our game
	// referenced in multiple game objects
	// for projection calculations
	Camera m_leftCam;

	// Right Camera for our game
	// referenced in multiple game objects
	// for projection calculations
	Camera m_rightCam;

	// unique pointer to our cube
	std::unique_ptr<Cube> m_cube;

	// timer since game started
	sf::Time m_gameTime;

	// input key handling
	KeyHandler m_keyhandler;

	// obj loading error message
	std::string m_objError;
};

#endif // !GAME